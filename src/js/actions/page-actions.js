var AppDispatcher = require('../dispatchers/app-dispatcher');

var Actions = {
	changePage: function (index) {
		AppDispatcher.dispatch({
			actionType:"CHANGE_PAGE",
			index:index
		});
	}
};

module.exports = Actions;
