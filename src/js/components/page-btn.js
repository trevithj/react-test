var React = require('react');
var Actions = require('../actions/page-actions');

var btnStyle={
	width:"3em",
	height: 30,
	backgroundColor:"orange"
};
var selStyle={
	width:"3em",
	height: 30,
	backgroundColor:"white",
	color: "silver"
};

function getStyle(isSel) {
	return (isSel) ? selStyle : btnStyle;
}

var Btn = React.createClass({
	handler: function () {
		Actions.changePage(this.props.index);
	},
	render:function () {
		return (
			<button onClick={this.handler} style={getStyle(this.props.selected)}>
				{this.props.index+1}
			</button>
		);
	}
});

module.exports = Btn;
