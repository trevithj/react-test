var React = require('react');
var Pages = require('./pages-list');
var Articles = require('./articles-list');
var mainStyle={
	width: "70%",
	marginLeft: "15%",
	paddingLeft: 20,
	fontFamily: "Arial, Helvetica, sans-serif",
	backgroundColor: "#eee"
};

var App = React.createClass({
	render:function () {
		return (
			<div style={mainStyle}>
				<h1>The Minimalist Article List</h1>
				<Pages />
				<Articles />
			</div>
		);
	}
});

module.exports = App;
