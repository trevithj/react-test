var React = require('react');
var Article = require("./article-item");
var Store = require('../stores/app-store');

var List = React.createClass({
	getInitialState:function () {
		return Store.getArticlesList();
	},
	componentWillMount: function() {
      Store.addChangeListener(this._onChange);
    },
	componentDidMount: function () {
		this._onChange();
	},
    componentWillUnmount: function() {
      Store.removeChangeListener(this._onChange);
    },
	render:function () {
		var items = this.state.articles.map(function (item, i) {
			return (
				<Article key={i} data={item} />
			);
		});
		return (<div>{items}</div>);
	},
	_onChange: function() {
		var self = this;
		console.log("Noting change to articles list");
		Store.loadPages(function (isOK) {
			if(isOK) {
				self.setState(Store.getArticlesList());
			} //else ignore
		});
	}
});


module.exports = List;
