var React = require('react');
var Btn = require("./page-btn");
var Store = require('../stores/app-store');
var divStyle={
	marginBottom: 5
};

var Pages = React.createClass({
	getInitialState:function () {
		return Store.getPagesList();
	},
	componentWillMount: function() {
      Store.addChangeListener(this._onChange);
    },
    componentWillUnmount: function() {
      Store.removeChangeListener(this._onChange);
    },
	render:function () {
		var sel = this.state.selected;
		var btns = this.state.pages.map(function (page, i) {
			return (
				<Btn key={i} selected={i==sel} index={i} />
			);
		});
		return (<div style={divStyle}> Page: {btns}</div>);
	},
	_onChange: function() {
		console.log("noting change to pages list", 25);
		this.setState(Store.getPagesList());
	}
});

module.exports = Pages;
