var React = require('react');

var divStyle={
	height: 110,
	paddingTop: 10,
	borderTop: "solid black thin"
};
var h3Style={
};
var imgStyle={
	float:"right",
	height: 100
};

var Article = React.createClass({
	render:function () {
		return (
			<div style={divStyle}>
				<img style={imgStyle} src={this.props.data.image} />
			 	<h3 style={h3Style}>{this.props.data.title}</h3>
			</div>
		);
	}
});

module.exports = Article;
