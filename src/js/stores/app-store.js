var AppDispatcher = require('../dispatchers/app-dispatcher');
var EventEmitter = require('events').EventEmitter;
var http = require("http");

var dataURL = "http://localhost:8888/react-test/dist/assets"; //change depending on deployment

var pagesList = {
	pages:["list1","list2","list3"],
	selected:0
};

var articlesList = {
	articles: []
};

var emitter = new EventEmitter();

var AppStore = {
	emitChange: function () {
		emitter.emit("change");
	},
	addChangeListener:function (callback) {
		emitter.on("change", callback);
	},
	removeChangeListener:function (callback) {
		emitter.removeListener("change", callback);
	},
	getPagesList: function () {
		return pagesList;
	},
	getArticlesList: function () {
		return articlesList;
	},
	loadPages: function(callback) {
		var index = pagesList.selected+1;
		var file = "/list"+index+".json";
		http.get(dataURL+file, function (res) {
			res.on("data", function (data) {
				articlesList.articles = JSON.parse(data);
				callback(true);
			});
			res.on("error", function function_name(err) {
				console.log("Error", err);
				callback(false);
			});
		});
	}
};


AppDispatcher.register(function(action) {
	if(action.actionType==="CHANGE_PAGE") {
		pagesList.selected = action.index;
		AppStore.emitChange();
	}
	return true;
});

module.exports = AppStore;
