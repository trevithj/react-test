# Dev Notes

## Assumptions made re design:

- 'loading more articles' doesn't mean appending to the list, but rather paging through the articles.
- we don't care about caching the loaded pages at this point
- no need to worry about reading any *content* of an article

## Issues

Some difficulty with an elusive bug when trying to load json via Ajax in the AppStore instance.
It seems that the 'close' event results in another 'close' event being emitted, resulting in an endless loop that quickly uses the stack up. I suspect that this has to do with the AppStore object extending EventEmitter.prototype, as per FB's suggested React Starter Kit approach. Instead of propagating down a hierarchy of event emitters, the calls loop within the same instance.

### Trial solution #1 ###
Move Ajax call into the component, and out of the Store instance. This seems to be common practice in online examples.

But doesn't work here. Same error.

### Trial solution #2 ###
Don't extend an EventEmitter for the store, try using an internal delegate instance instead.

Eureka! Seems the problem was as suspected. Now I can move the `http` call back into AppStore, and have the article-list component update its state on a successful callback.

And ... app is now functional!

### Unit testing for React on Node ###
The `jest` package wouldn't install on my machine, so I used the `jasmine` package as a fallback. Implemented unit tests on the Action -> Dispatcher -> Store cycle, and made sure relevant state objects were being returned as expected.
