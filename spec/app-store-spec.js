'use strict';

var React = require('react/addons');
var Store = require("../src/js/stores/app-store");
var Actions = require("../src/js/actions/page-actions");
var TestUtils = React.addons.TestUtils;

describe("AppStore test", function () {
	it("should exist", function () {
		expect(Store).toBeDefined();
	});
	it("should return pages list", function () {
		var pl = Store.getPagesList();
		expect(pl.selected).toEqual(0);
		expect(pl.pages.length).toBeGreaterThan(1);
	});

	it("should return empty articles list at first", function () {
		var al = Store.getArticlesList();
		expect(al.articles).toEqual([]);
	});

	describe("Actions test", function () {
		beforeEach(function(done) {
			Store.addChangeListener(function () {
				done();
			})
			Actions.changePage(2);
		});
		it("should update selected page and fire change event", function (done) {
			var s = Store.getPagesList().selected;
			expect(s).toEqual(2);
			done();
		});
	});

	describe("Load page test", function () {
		beforeEach(function(done) {
			Store.loadPages(function (isOK) {
				done();
			});
		});
		it("should return populated articles list", function () {
			var al = Store.getArticlesList();
			expect(al.articles.length).toBeGreaterThan(0);
		})
	});


});
